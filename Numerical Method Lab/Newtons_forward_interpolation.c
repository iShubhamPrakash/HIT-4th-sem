/*
DATE: 22.02.19

Newtons forward interpolation rule-
x=0.5
n=4
x: 0  1    2    3    4  
y: 1  1.5  2.2  3.1  4.3

h=1
Ans= 1.221
*/

#include <stdio.h>
#include <conio.h>

int main()
{
  float x, x0,h,u,s=0,p,y[10][10];
  int n,i,j;
  
  printf("\nEnter the value of x, x0, h, and n \n");
  scanf("%f%f%f%d",&x,&x0,&h,&n);
  
  printf("\nEnter the value of y0 to y%d \n",n);
  
  for(i=0; i<=n; i++)
  scanf("%f",&y[0][i]);
  
  u=(x-x0)/h;
  
  for(i=1;i<=n;i++){
     for(j=0; j<=n-i; j++)
     {
        y[i][j]=y[i-1][j+1] -y[i-1][j];
     }
  }
  s=y[0][0];
  p=1;
  
  for(i=1;i<=n;i++)
  {
     p *= (u-i+1)/i;
     s += p*y[i][0];
  }
  
  printf("/n Sum= %f",s);
  getch();	
  return 0;
}

